import React from "react";

class SearchBar extends React.Component {
	state = { term: "" };

	onSubmitForm(e) {
		e.preventDefault();

		// if we are in a class component we reference the props with this.
		this.props.onSubmit(this.state.term);
	}

	render() {
		return (
			<div className="search-bar ui segment">
				<form
					className="ui form"
					onSubmit={(e) => this.onSubmitForm(e)}
				>
					<div className="field">
						<label>Search</label>
						<input
							type="text"
							value={this.state.term}
							onChange={(e) => {
								this.setState({ term: e.target.value });
								console.log(e.target.value);
							}}
						/>
					</div>
				</form>
			</div>
		);
	}
}

export default SearchBar;
