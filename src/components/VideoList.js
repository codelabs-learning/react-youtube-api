import React from "react";
import VideoItem from "./VideoItem";

const VideoList = ({ videos, onVideoSelect }) => {
	let videoItems = videos.map((video) => {
		console.log(video);
		return (
			<VideoItem
				key={video.etag}
				video={video}
				onVideoSelect={onVideoSelect}
			/>
		);
	});

	return <div className="ui relaxed divided list">{videoItems}</div>;
};

export default VideoList;
