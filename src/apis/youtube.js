import axios from "axios";
const KEY = "AIzaSyC4jBXelyhBPuXt2wS8uXNgAr-SsxUA-N0";

//Preconfigure axios object.

export default axios.create({
	baseURL: "https://www.googleapis.com/youtube/v3",
	params: {
		part: "snippet",
		maxResults: 5,
		type: "video",
		key: KEY,
	},
});
